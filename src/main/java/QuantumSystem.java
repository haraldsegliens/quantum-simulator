
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class QuantumSystem {

    private static final double ROUNDING = 0.0000000000000001;

    private final int qubits;
    private final int systemSize;
    private Matrix system;
    private Matrix unitaryMatrix;

    public QuantumSystem(final int qubitCount) {
        this.qubits = qubitCount;
        this.systemSize = (int) Math.pow(2, qubitCount);
        initializeSystem(new Matrix(() -> {
            double[][] states = new double[1][systemSize];
            Arrays.fill(states[0], 0.0);
            states[0][0] = 1.0;
            return states;
        }));
    }

    private void initializeSystem(Matrix initialState) {
        this.system = initialState;
        this.unitaryMatrix = new Matrix(() -> {
            double[][] unitaryMatrixArray = new double[systemSize][systemSize];
            for(int i = 0; i < systemSize; i++) {
                Arrays.fill(unitaryMatrixArray[i], 0.0);
                unitaryMatrixArray[i][i] = 1.0;
            }
            return unitaryMatrixArray;
        });
    }

    public QuantumSystem applyGate(final Matrix gate, final int... wires) {

        Matrix masterGate = createEmptyMatrix();

        int[] sortedWires = Arrays.copyOf(wires, wires.length);
        Arrays.sort(sortedWires);

        for (int row = 0; row < systemSize; row++) {
            for (int col = 0; col < systemSize; col++) {

                // Code based on:
                // https://github.com/RoboNeo9/Java-Quantum-Computer-Simulator/blob/master/Core/MasterGate.java

                // First we look up which qubits are NOT part of the wires of this gate:
                int rowQubitsNotWire = 0;
                int colQubitsNotWire = 0;

                for (int i = 0; i < qubits; i++) {
                    // If this is NOT part of the wire, add these bits to our bitstring:
                    if (Arrays.binarySearch(sortedWires, i) < 0) {
                        // Shift everything and add the current row and col bit:
                        rowQubitsNotWire = (rowQubitsNotWire << 1) | ((row >> i) & 1);
                        colQubitsNotWire = (colQubitsNotWire << 1) | ((col >> i) & 1);
                    }
                }

                if (rowQubitsNotWire == colQubitsNotWire) {

                    int rowQubitsWire = 0;
                    int colQubitsWire = 0;

                    // Beware, the order of wires here is important (in relation to the gate), reverse traversal matters
                    for (int i = wires.length; i-- > 0; ) {
                        int wire = wires[i];
                        // Shift everything and add the current row and col bit:
                        rowQubitsWire = (rowQubitsWire << 1) | ((row >> wire) & 1);
                        colQubitsWire = (colQubitsWire << 1) | ((col >> wire) & 1);
                    }

                    double data = gate.getData()[rowQubitsWire][colQubitsWire];
                    masterGate.setEntry(row, col, data);
                }
            }
        }

        system = Matrix.multiply(system, masterGate);
        unitaryMatrix = Matrix.multiply(unitaryMatrix, masterGate);

        return this;
    }

    private Matrix createEmptyMatrix() {
        return new Matrix(systemSize, systemSize);
    }

    private void prettyPrintMatrix(final Matrix masterGate) {
        for (int i = 0; i < masterGate.getRowDimension(); i++) {
            System.out.print("\t");
            for (int z = 0; z < masterGate.getColumnDimension(); z++) {
                System.out.printf("%.2f", masterGate.getData()[i][z]);
                System.out.print("  ");
                if(masterGate.getData()[i][z] >= 0) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public QuantumSystem printUnitaryMatrix() {
        System.out.println("Unitary matrix:");
        prettyPrintMatrix(unitaryMatrix);
        return this;
    }

    public Matrix getUnitaryMatrix() {
        return unitaryMatrix;
    }

    public QuantumSystem printState() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("Current state of system:")
                .append(System.lineSeparator());
        for (int ptr = 0; ptr < systemSize; ptr++) {
            stringBuilder
                    .append("\t")
                    .append(toBinaryString(ptr))
                    .append(": ")
                    .append(String.format("%.2f", system.getData()[0][ptr]));
            if (ptr < systemSize - 1) {
                stringBuilder.append(System.lineSeparator());
            }
        }
        System.out.println(stringBuilder);
        return this;
    }

    private Matrix getState() {
        return system;
    }

    public Matrix getObservedProbabilities() {
        Matrix result = new Matrix(1, systemSize);
        for (int ptr = 0; ptr < systemSize; ptr++) {
            result.setEntry(0, ptr, Math.pow(system.getData()[0][ptr], 2));
        }
        return result;
    }

    public Matrix getMeasuredState() {
        Matrix observedProbabilities = getObservedProbabilities();

        Matrix result = new Matrix(1, systemSize);
        RandomCollection<Integer> randomCollection = new RandomCollection<>();
        for (int ptr = 0; ptr < systemSize; ptr++) {
            randomCollection.add(observedProbabilities.getData()[0][ptr], ptr);
        }
        result.setEntry(0, randomCollection.next(), 1.0);
        return result;
    }

    public QuantumSystem measure() {
        Matrix measuredState = getMeasuredState();
        initializeSystem(measuredState);
        return this;
    }

    public QuantumSystem printObservedProbabilities() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("Observed probabilities are:")
                .append(System.lineSeparator());
        for (int ptr = 0; ptr < systemSize; ptr++) {
            stringBuilder
                    .append("\t")
                    .append(toBinaryString(ptr))
                    .append(": ")
                    .append(String.format("%.2f", Math.pow(system.getData()[0][ptr], 2) * 100))
                    .append("%");
            if (ptr < systemSize - 1) {
                stringBuilder.append(System.lineSeparator());
            }
        }
        System.out.println(stringBuilder);
        return this;
    }

    private String toBinaryString(final int measuredState) {
        return String.format("%0" + qubits + "d", Integer.parseInt(Integer.toBinaryString(measuredState)));
    }

    public Map<String, Integer> execute(int numberOfShots) {
        HashMap<String, Integer> result = new HashMap<>();

        for(int i = 0; i < numberOfShots; i++) {
            Matrix measuredState = getMeasuredState();
            for(int j = 0; j < systemSize; j++) {
                if(measuredState.getData()[0][j] == 1.0) {
                    result.compute(toBinaryString(j), (k, v) -> (v == null) ? 1 : v + 1);
                    break;
                }
            }
        }

        return result;
    }
}
