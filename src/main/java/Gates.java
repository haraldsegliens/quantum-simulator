public class Gates {

    public static final Matrix IDENTITY_GATE =
            new Matrix(new double[][] {
                    {1.0, 0.0},
                    {0.0, 1.0}
            });

    public static final Matrix NOT =
            new Matrix(new double[][] {
                    {0.0, 1.0},
                    {1.0, 0.0}
            });

    public static final Matrix Z_GATE =
            new Matrix(new double[][] {
                    {1.0, 0.0},
                    {0.0, -1.0}
            });

    public static final Matrix H =
            new Matrix(new double[][] {
                    {1/Math.sqrt(2), 1/Math.sqrt(2)},
                    {1/Math.sqrt(2), -1/Math.sqrt(2)}
            });

    public static Matrix ROTATION(double angleInRadians) {
        return new Matrix(new double[][] {
                {Math.cos(angleInRadians), -Math.sin(angleInRadians)},
                {Math.sin(angleInRadians), Math.cos(angleInRadians)}
        });
    }

    public static final Matrix CNOT =
            new Matrix(new double[][] {
                    {1.0, 0.0, 0.0, 0.0},
                    {0.0, 1.0, 0.0, 0.0},
                    {0.0, 0.0, 0.0, 1.0},
                    {0.0, 0.0, 1.0, 0.0}
            });

    public static final Matrix CZ =
            new Matrix(new double[][] {
                    {1.0, 0.0, 0.0, 0.0},
                    {0.0, 1.0, 0.0, 0.0},
                    {0.0, 0.0, 1.0, 0.0},
                    {0.0, 0.0, 0.0, -1.0}
            });

}
