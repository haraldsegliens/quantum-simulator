import java.util.Map;

public class QuantumSimulatorTest {
    public static void main(String[] args) {
        new QuantumSimulatorTest().run();
    }

    private void run() {

        System.out.println("Quantum circuit with H gate.");
        QuantumSystem quantumSystem = new QuantumSystem(2)
                .applyGate(Gates.H, 0)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts = quantumSystem.execute(1000);
        counts.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();


        System.out.println("Quantum circuit with X gate.");
        QuantumSystem quantumSystem2 = new QuantumSystem(2)
                .applyGate(Gates.NOT, 0)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts2 = quantumSystem2.execute(1000);
        counts2.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Quantum circuit with Z gate.");
        QuantumSystem quantumSystem3 = new QuantumSystem(2)
                .applyGate(Gates.Z_GATE, 0)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts3 = quantumSystem3.execute(1000);
        counts3.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Quantum circuit with ROTATION gate.");
        QuantumSystem quantumSystem4 = new QuantumSystem(1)
                .applyGate(Gates.ROTATION(Math.toRadians(30)), 0)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts4 = quantumSystem4.execute(1000);
        counts4.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Quantum circuit with CNOT gate.");
        QuantumSystem quantumSystem5 = new QuantumSystem(2)
                .applyGate(Gates.CNOT, 0,1)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts5 = quantumSystem5.execute(1000);
        counts5.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Quantum circuit with CZ gate.");
        QuantumSystem quantumSystem6 = new QuantumSystem(2)
                .applyGate(Gates.CZ, 0,1)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts6 = quantumSystem6.execute(1000);
        counts6.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Quantum circuit with phase kickback.");
        QuantumSystem quantumSystem7 = new QuantumSystem(2)
                .applyGate(Gates.NOT, 0)
                .applyGate(Gates.H, 0)
                .applyGate(Gates.H, 1)
                .applyGate(Gates.CNOT, 0, 1)
                .applyGate(Gates.H, 0)
                .applyGate(Gates.H, 1)
                .printObservedProbabilities()
                .printState()
                .printUnitaryMatrix();

        System.out.println("Executed the quantum system for 1000 times");
        Map<String, Integer> counts7 = quantumSystem7.execute(1000);
        counts7.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
        System.out.println();
        System.out.println();

    }
}
