import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.Supplier;

public class Matrix {
    final private double[][] data;

    public Matrix(double[][] data) {
        this.data = data;
    }

    public Matrix(Supplier<double[][]> dataSupplier) {
        this(dataSupplier.get());
    }

    public Matrix(int rows, int cols) {
        this(() -> {
            double[][] array = (double[][]) Array.newInstance(double.class,
                    new int[] {
                            rows, cols
                    });
            for (int i = 0; i < rows; ++i) {
                Arrays.fill(array[i], 0.0);
            }
            return array;
        });
    }

    public double[][] getData() {
        return data;
    }

    public void setEntry(int row, int col, double entry) {
        data[row][col] = entry;
    }

    public int getRowDimension() {
        return data.length;
    }

    public int getColumnDimension() {
        return data[0].length;
    }

    public static Matrix multiply(final Matrix n, final Matrix m) {
        final int nRows = n.getRowDimension();
        final int nCols = m.getColumnDimension();
        final int nSum = n.getColumnDimension();
        final Matrix out = new Matrix(nRows, nCols);
        for (int row = 0; row < nRows; ++row) {
            for (int col = 0; col < nCols; ++col) {
                double sum = 0.0;
                for (int i = 0; i < nSum; ++i) {
                    sum = sum + (n.getData()[row][i] * m.getData()[i][col]);
                }
                out.setEntry(row, col, sum);
            }
        }
        return out;
    }
/*
    public static Matrix tensorProduct(Matrix lhs, Matrix rhs) {
        int r1 = lhs.getRowDimension();
        int c1 = lhs.getColumnDimension();
        int r2 = rhs.getRowDimension();
        int c2 = rhs.getColumnDimension();
        Matrix result = new Array2DRowFieldMatrix<>(Decimal64Field.getInstance(), r1 * r2, c1 * c2);
        for (int i = 0; i < r1; i++) {
            for (int j = 0; j < c1; j++) {
                for (int k = 0; k < r2; k++) {
                    for (int l = 0; l < c2; l++) {
                        int row = i * r2 + k, col = j * c2 + l;
                        result.setEntry(row, col, lhs.getEntry(i, j).multiply(rhs.getEntry(k, l)));
                    }
                }
            }
        }
        return result;
    }*/
}
